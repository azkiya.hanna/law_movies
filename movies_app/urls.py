from django.urls import path
from . import views

app_name = 'movies_app'

urlpatterns = [
    # path('', views.index, name='index'),
    path('movies/', views.get_post_movies),
    path('movie/<int:pk>/', views.get_movie),
    path('picture/', views.post_picture),
    path('actors/', views.get_movies_and_actors),
]