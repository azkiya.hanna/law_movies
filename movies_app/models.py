from pyexpat import model
from django.db import models

class Movie(models.Model):
    title = models.CharField(max_length=500)
    rating = models.FloatField()
    genre = models.CharField(max_length=10)

class Picture(models.Model):
    picture_uploaded = models.ImageField(upload_to='images/')

class Actor(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    name = models.CharField(max_length=500)
    nationality = models.CharField(max_length=20)