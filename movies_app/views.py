from django.shortcuts import render

from rest_framework import status
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view

from django.http.response import JsonResponse

from .models import Movie, Picture, Actor
from .serializer import ActorSerializer, MovieSerializer, PictureSerializer

@api_view(['GET','POST'])
def get_post_movies(request):
    if request.method == 'GET':
        movie_list_serialized = MovieSerializer(Movie.objects.all(),many=True)
        return JsonResponse(movie_list_serialized.data, status=status.HTTP_200_OK,safe=False)
    if request.method == 'POST':
        title = request.data.get("title")
        rating = request.data.get("rating")
        genre = request.data.get("genre")
        movie = Movie.objects.create(
            title=title, rating = rating, genre=genre
        )
        return JsonResponse(MovieSerializer(movie).data, status=status.HTTP_200_OK,safe=False)
        # movie_data_serialized = MovieSerializer(data=request.POST)
        # if movie_data_serialized.is_valid():
        #     movie_data_serialized.save()
        #     return JsonResponse(movie_data_serialized.data, status=status.HTTP_200_OK,safe=False)
    return JsonResponse({'message' : 'Terdapat kesalahan'}, status=status.HTTP_409_CONFLICT)
@api_view(['GET'])
def get_movies_and_actors(request):
    if request.method == 'GET':
        actors = ActorSerializer(Actor.objects.select_related().filter(nationality='British'),many=True)
        return JsonResponse(actors.data, status=status.HTTP_200_OK,safe=False)

@api_view(['GET','PUT','DELETE'])
def get_movie(request,pk):
    try: 
        movie = Movie.objects.get(id=pk)
    except:
        return JsonResponse({'message' : 'Film tidak dapat ditemukan'}, status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        return JsonResponse(MovieSerializer(movie).data, status=status.HTTP_200_OK,safe=False)
    elif request.method == 'PUT':
        rating = request.POST.get('rating')
        genre = request.POST.get('genre')
        if rating is not None : movie.rating = rating
        if genre is not None : movie.genre = genre      
        movie.save()
        return JsonResponse(MovieSerializer(movie).data, status=status.HTTP_200_OK,safe=False)
    elif request.method == 'DELETE':
        movie.delete()
        return JsonResponse({'message': 'Film berhasil dihapus'},status=status.HTTP_200_OK)

@api_view(['POST'])
def post_picture(request):
    try:
        picture = request.data['picture']
    except:
        return JsonResponse({'message' : 'Terdapat kesalahan'}, status=status.HTTP_400_BAD_REQUEST)
    picture = Picture.objects.create(picture_uploaded= picture)
    return JsonResponse(PictureSerializer(picture).data, status=status.HTTP_200_OK)
