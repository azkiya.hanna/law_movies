from django.contrib import admin

from .models import Actor, Movie, Picture
# Register your models here.
admin.site.register(Movie)

admin.site.register(Picture)
admin.site.register(Actor)